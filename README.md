# README #

This project uses CocoaPods. You'll need to install CocoaPods in order to download the project's dependencies. After you've installed CocoaPods, navigate to the project folder and run:
```
#!bash

pod install
```

After doing this you'll be able to compile the app.


# What it looks like

![Preview](http://s4.postimg.org/6cxpfqdel/i_OS_Simulator_Screen_shot_12_Jul_2014_16_55_35.png)
![Preview](http://s4.postimg.org/8rpl05tnh/i_OS_Simulator_Screen_shot_12_Jul_2014_16_56_12.png)
![Preview](http://s4.postimg.org/6of5shtul/i_OS_Simulator_Screen_shot_12_Jul_2014_17_00_53.png)
![Preview](http://s4.postimg.org/ctw95psiz/Fuzzy.gif)
![Preview](http://s29.postimg.org/x8prgl9sl/converted.gif)
