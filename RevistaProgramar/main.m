//
//  main.m
//  RevistaProgramar
//
//  Created by KTachyon on 05/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "DDTTYLogger.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
