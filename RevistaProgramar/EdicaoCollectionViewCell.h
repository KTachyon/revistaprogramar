//
//  EdicaoCollectionViewCell.h
//  RevistaProgramar
//
//  Created by KTachyon on 12/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EdicaoViewProtocol.h"

@interface EdicaoCollectionViewCell : UICollectionViewCell <EdicaoViewProtocol>

@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;

- (void)buildCellWithEdition:(NSDictionary *)edicao;

@end
