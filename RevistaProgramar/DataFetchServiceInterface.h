//
//  DataFetchServiceInterface.h
//  RevistaProgramar
//
//  Created by KTachyon on 24/08/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataFetchServiceInterface <NSObject>

- (PMKPromise *)fetchMetadata;
- (PMKPromise *)fetchPDFFileForEdicao:(NSDictionary *)edicao;

@end
