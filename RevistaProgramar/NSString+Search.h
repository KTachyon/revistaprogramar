//
//  NSString+Search.h
//  PremiumDex
//
//  Created by KTachyon on 18/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StringScore/NSString+Score.h>

@interface NSString (Search)

- (NSString *)toASCII;
- (CGFloat)fuzzyScore:(NSString *)search;

@end
