//
//  AppDelegate.h
//  RevistaProgramar
//
//  Created by KTachyon on 05/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
