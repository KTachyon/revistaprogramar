//
//  DataFetchService.m
//  RevistaProgramar
//
//  Created by KTachyon on 24/08/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import "DataFetchService.h"
#import "DataFetchServiceInterface.h"

@interface DataFetchService () <DataFetchServiceInterface>

@end

@implementation DataFetchService
objection_register_singleton(DataFetchService)

- (PMKPromise *)fetchMetadata {
    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject) {
        dispatch_promise(^{
            return [NSURLConnection GET:@"http://programar.sergioribeiro.com/programar.xml"];
        }).then(^(NSData *data) {
            return [NSDictionary dictionaryWithXMLData:data];
        }).then(^(NSDictionary *xmlDoc) {
            fulfill([xmlDoc valueForKeyPath:@"edicoes.edicao"]);
        }).catch(^(NSError *error) {
            // List could not be fetched!
            reject(error);
        });
    }];
}

- (PMKPromise *)fetchPDFFileForEdicao:(NSDictionary *)edicao {
    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject) {
        dispatch_promise(^{
            return [NSURLConnection GET:[edicao valueForKeyPath:@"pdf"]];
        }).then(^(NSData *data) {
            fulfill(data);
        }).catch(^(NSError *error) {
            // PDF could not be downloaded!
            reject(error);
        });
    }];
}

@end
