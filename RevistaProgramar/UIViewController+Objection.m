//
//  UIViewController+Objection.m
//  LogMyTime
//
//  Created by KTachyon on 01/03/14.
//  Copyright (c) 2014 Premium Minds. All rights reserved.
//

#import "UIViewController+Objection.h"

@implementation UIViewController (Objection)

- (void) awakeFromNib {
    [[JSObjection defaultInjector] injectDependencies:self];
}

@end
