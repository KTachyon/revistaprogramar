//
//  EdicoesCollectionViewController.m
//  RevistaProgramar
//
//  Created by KTachyon on 12/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import "EdicoesCollectionViewController.h"
#import "EdicaoCollectionViewCell.h"
#import "EdicaoViewController.h"
#import "EdicaoCarouselView.h"

#import "DataFetchServiceInterface.h"

#import "NSString+Search.h"

#import <iCarousel/iCarousel.h>

@interface EdicoesCollectionViewController () <UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, iCarouselDelegate, iCarouselDataSource> {
    NSArray *_edicoes;
    NSString *_filter;
    
    NSArray *_searchArray;
    NSString *_filterForSearchArray;
}

@property (nonatomic) UISearchBar *searchBar;
@property (nonatomic) NSString *filter;

@property (nonatomic) id<DataFetchServiceInterface> dataFetchService;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet iCarousel *carousel;

@end

@implementation EdicoesCollectionViewController
objection_requires_sel(@selector(dataFetchService))

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.collectionView.frame), 44)];
    self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchBar.keyboardType = UIKeyboardTypeASCIICapable;
    self.searchBar.delegate = self;
    
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchBar.barStyle = UIBarStyleBlack;
    self.searchBar.translucent = YES;
    
    [self.collectionView addSubview:self.searchBar];
    
    [self configureSubviewTextField:self.searchBar withBlock:^(UITextField *textField) {
        [textField setKeyboardAppearance:UIKeyboardAppearanceDark];
        [textField setReturnKeyType:UIReturnKeyDone];
        [textField setEnablesReturnKeyAutomatically:NO];
    }];
    
    // Do any additional setup after loading the view.
    _edicoes = [NSArray new];
    
    dispatch_promise(^{
        return [_dataFetchService fetchMetadata];
    }).then(^(NSArray *edicoes) {
        _edicoes = edicoes;
        [_collectionView reloadData];
        [_carousel reloadData];
    }).catch(^(NSError *error) {
        // List could not be fetched!
        DDLogError(@"Bad stuff happened: %@", error);
    });
    
    _carousel.type = iCarouselTypeTimeMachine;
    
    self.title = @"Edições";
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

- (void)setFilter:(NSString *)searchText {
    NSArray *current = [self filtered];
    
    _filter = searchText;
    
    NSArray *next = [self filtered];
    NSArray *disappear = Underscore.without(current, next);
    NSArray *appear = Underscore.without(next, current);
    
    NSMutableArray *indexesOfCurrentToDisappear = [NSMutableArray array];
    
    for (id obj in disappear) {
        [indexesOfCurrentToDisappear addObject:[NSIndexPath indexPathForRow:Underscore.indexOf(current, obj) inSection:0]];
    }
    
    NSMutableArray *indexesOfNextToAppear = [NSMutableArray array];
    
    for (id obj in appear) {
        [indexesOfNextToAppear addObject:[NSIndexPath indexPathForRow:Underscore.indexOf(next, obj) inSection:0]];
    }
    
    [self.collectionView performBatchUpdates:^{
        [_collectionView deleteItemsAtIndexPaths:indexesOfCurrentToDisappear];
        [_collectionView insertItemsAtIndexPaths:indexesOfNextToAppear];
    } completion:nil];
}

- (NSArray *)filtered {
    
    if ([_filterForSearchArray isEqualToString:self.filter]) {
        if (!_searchArray) {
            _searchArray = _edicoes;
        }
        
        return _searchArray;
    }
    
    BOOL (^fuzzyMembro)(NSString *) = ^(NSString *membro) {
        return (BOOL)([membro fuzzyScore:self.filter] > 0.7);
    };
    
    BOOL (^fuzzyArtigo)(NSDictionary *) = ^(NSDictionary *artigo) {
        BOOL titulo = [[artigo attributes][@"titulo"] fuzzyScore:self.filter] > 0.7;
        BOOL description = [[artigo innerText] fuzzyScore:self.filter] > 0.7;
        
        return (BOOL) (titulo || description);
    };
    
    BOOL (^fuzzyCategoria)(NSString *) = ^(NSString *categoria) {
        if ([[categoria valueForKeyPath:@"artigo"] isKindOfClass:[NSArray class]]) {
            return Underscore.any([categoria valueForKeyPath:@"artigo"], fuzzyArtigo);
        }
        
        return fuzzyArtigo([categoria valueForKeyPath:@"artigo"]);
    };
    
    NSArray *matchFilter = Underscore.filter(_edicoes, ^(NSDictionary *edicao) {
        if (self.filter && [self.filter length] > 0) {
        
            if (Underscore.any([edicao valueForKeyPath:@"equipa.membro"], fuzzyMembro)) {
                return YES;
            }
            
            if (Underscore.any([edicao valueForKeyPath:@"artigos.categoria"], fuzzyCategoria)) {
                return YES;
            }
            
            
        } else {
            return YES;
        }
        
        return NO;
    });
    
    _searchArray = matchFilter;
    _filterForSearchArray = self.filter;
    
    return _searchArray;
}

- (NSString *)filter {
    return _filter;
}

#warning Temporary hack to be able to configure UISearchBar's UITextField
- (void)configureSubviewTextField:(UIView *)view withBlock:(void (^)(UITextField *))configurationBlock {
    for(UIView *subView in [view subviews]) {
        if([subView conformsToProtocol:@protocol(UITextInputTraits)]) {
            configurationBlock((UITextField *)subView);
        } else {
            [self configureSubviewTextField:subView withBlock:configurationBlock];
        }
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.filter = searchText;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self filtered].count;
}

- (EdicaoCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"EdicaoCellIdentifier";
    
    EdicaoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell buildCellWithEdition:[[self filtered] objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //return CGSizeMake(237, 335);
    CGFloat width = (collectionView.frame.size.width - 4.0) / 3;
    CGFloat height = width * 1.4;
    
    return CGSizeMake(width, height);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id<EdicaoViewProtocol>)sender {
    [self.searchBar resignFirstResponder];
    
    EdicaoViewController *evc = [segue destinationViewController];
    evc.edicao = sender.edicao;
}

#pragma mark - iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return [_edicoes count];
}

- (EdicaoCarouselView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(EdicaoCarouselView *)view {
    
    NSDictionary *edicao = [_edicoes objectAtIndex:index];
    
    if (view == nil) {
        view = [[EdicaoCarouselView alloc] initWithFrame:CGRectMake(0, 0, 150.0f, 210.0f)];
    }
    
    [view buildCellWithEdition:edicao];
    
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    [self performSegueWithIdentifier:@"EdicaoCarouselPushSegueIdentifier" sender:[carousel itemViewAtIndex:index]];
}


#pragma mark - Handle rotation events

- (void)orientationChanged:(NSNotification *)notification
{
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(deviceOrientation)) {
        [_collectionView setHidden:YES];
    }
    else if (deviceOrientation == UIDeviceOrientationPortrait) {
        [_collectionView setHidden:NO];
    }
}

@end
