//
//  NSString+Search.m
//  PremiumDex
//
//  Created by KTachyon on 18/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import "NSString+Search.h"
#import <StringScore/NSString+Score.h>

@implementation NSString (Search)

- (NSString *)toASCII {
    NSData *asciiData = [self dataUsingEncoding:NSASCIIStringEncoding
                           allowLossyConversion:YES];
    
    NSString *asciiString = [[NSString alloc] initWithData:asciiData
                                                  encoding:NSASCIIStringEncoding];
    
    return [asciiString lowercaseString];
}

- (CGFloat)fuzzyScore:(NSString *)search {
    NSString *asciiSearch = [search toASCII];
    NSString *asciiString = [self toASCII];
    
    NSArray* components = [asciiString componentsSeparatedByString:@" "];
    
    CGFloat maxScore = [asciiString scoreAgainst:asciiSearch];
    
    for (NSString *component in components) {
        CGFloat score = [component scoreAgainst:asciiSearch];
        if (score > maxScore) { maxScore = score; }
    }
    
    return maxScore;
}

@end
