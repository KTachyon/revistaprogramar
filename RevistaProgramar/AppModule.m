//
//  AppModule.m
//  RevistaProgramar
//
//  Created by KTachyon on 23/08/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import "AppModule.h"

#import "DataFetchService.h"
#import "DataFetchServiceInterface.h"

@implementation AppModule

- (void)configure {
    [self bindBlock:^id(JSObjectionInjector *context) {
        DDLogVerbose(@"DataFetchService Injection");
        return [context getObject:[DataFetchService class]];
    } toProtocol:@protocol(DataFetchServiceInterface)];
}

@end
