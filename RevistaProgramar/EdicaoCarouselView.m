//
//  EdicaoCarouselView.m
//  RevistaProgramar
//
//  Created by KTachyon on 13/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import "EdicaoCarouselView.h"

@interface EdicaoCarouselView () {
    NSDictionary *_edicao;
}

@end

@implementation EdicaoCarouselView

- (void)buildCellWithEdition:(NSDictionary *)edicao {
    self.image = nil;
    self.contentMode = UIViewContentModeScaleToFill;
    
    _edicao = edicao;
    
    dispatch_promise(^{
        NSArray *capas = [edicao valueForKeyPath:@"imagens.capas.capa"];
        
        NSDictionary* capa = Underscore.find(capas, ^BOOL (NSDictionary *capa) {
            return [[capa attributes][@"tamanho"] isEqualToString:@"grande"];
        });
        
        return [capa innerText];
    }).thenOn(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(NSString *url) {
        return [NSURLConnection GET:url];
    }).thenOn(dispatch_get_main_queue(), ^(UIImage *image) {
        if (_edicao == edicao) {
            self.image = image;
        }
    }).catch(^(NSError *error) {
        // Image cannot be displayed
        DDLogError(@"Bad stuff happened: %@", error);
    });
}

- (NSDictionary *)edicao {
    return _edicao;
}

@end
