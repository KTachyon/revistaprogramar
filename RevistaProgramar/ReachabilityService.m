//
//  ReachabilityService.m
//  RevistaProgramar
//
//  Created by KTachyon on 19/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import "ReachabilityService.h"
#import <Reachability/Reachability.h>

@implementation ReachabilityService
objection_register_singleton(ReachabilityService)

- (void)setup {
    // Allocate a reachability object
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    reach.reachableBlock = ^(Reachability *reach) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"REACHABLE!");
        });
    };
    
    reach.unreachableBlock = ^(Reachability *reach) {
        NSLog(@"UNREACHABLE!");
    };
    
    [reach startNotifier];
}

@end
