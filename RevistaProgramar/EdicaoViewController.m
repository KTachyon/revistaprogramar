//
//  EdicaoViewController.m
//  RevistaProgramar
//
//  Created by KTachyon on 12/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import "EdicaoViewController.h"
#import "DataFetchServiceInterface.h"

#define kTopPadding 64

@interface EdicaoViewController () <UIWebViewDelegate>

@property (nonatomic) id<DataFetchServiceInterface> dataFetchService;

@end

@implementation EdicaoViewController
objection_requires_sel(@selector(dataFetchService))

@synthesize edicao;
@synthesize pdfView;
@synthesize loadingIndicatorView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    pdfView.scrollView.contentInset = UIEdgeInsetsMake(kTopPadding, 0, 0, 0);
    pdfView.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(kTopPadding, 0, 0, 0);
    
    self.title = [NSString stringWithFormat:@"Edição nº%@", [edicao valueForKeyPath:@"_num"]];
    
    dispatch_promise_on(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        return [_dataFetchService fetchPDFFileForEdicao:edicao];
    }).thenOn(dispatch_get_main_queue(), ^(NSData *data) {
        [pdfView loadData:data MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:nil];
    }).catch(^(NSError *error) {
        // PDF could not be downloaded!
        DDLogError(@"Bad stuff happened: %@", error);
    });
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    loadingIndicatorView.hidden = YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [pdfView.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    });
}

@end
