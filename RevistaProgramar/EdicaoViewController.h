//
//  EdicaoViewController.h
//  RevistaProgramar
//
//  Created by KTachyon on 12/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EdicaoViewController : UIViewController

@property (nonatomic) NSDictionary *edicao;
@property (nonatomic, weak) IBOutlet UIWebView *pdfView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingIndicatorView;

@end
