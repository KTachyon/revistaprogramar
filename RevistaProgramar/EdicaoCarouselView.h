//
//  EdicaoCarouselView.h
//  RevistaProgramar
//
//  Created by KTachyon on 13/07/14.
//  Copyright (c) 2014 KTachyon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EdicaoViewProtocol.h"

@interface EdicaoCarouselView : UIImageView <EdicaoViewProtocol>

- (void)buildCellWithEdition:(NSDictionary *)edicao;

@end
